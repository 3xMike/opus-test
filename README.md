# opus test
```bash
cargo build --target=x86_64-unknown-linux-gnu
```
# Problem
```
Caused by:
  process didn't exit successfully: `C:\Users\user\Rust\test_opus\target\debug\build\opus-sys-4908b52128f44c97\build-script-build` (exit code: 101)
  --- stdout
  cargo:rerun-if-env-changed=OPUS_NO_PKG_CONFIG
  cargo:rerun-if-env-changed=PKG_CONFIG_ALLOW_CROSS_x86_64-unknown-linux-gnu
  cargo:rerun-if-env-changed=PKG_CONFIG_ALLOW_CROSS_x86_64_unknown_linux_gnu
  cargo:rerun-if-env-changed=TARGET_PKG_CONFIG_ALLOW_CROSS
  cargo:rerun-if-env-changed=PKG_CONFIG_ALLOW_CROSS
  cargo:rerun-if-env-changed=PKG_CONFIG_x86_64-unknown-linux-gnu
  cargo:rerun-if-env-changed=PKG_CONFIG_x86_64_unknown_linux_gnu
  cargo:rerun-if-env-changed=TARGET_PKG_CONFIG
  cargo:rerun-if-env-changed=PKG_CONFIG
  cargo:rerun-if-env-changed=PKG_CONFIG_SYSROOT_DIR_x86_64-unknown-linux-gnu
  cargo:rerun-if-env-changed=PKG_CONFIG_SYSROOT_DIR_x86_64_unknown_linux_gnu
  cargo:rerun-if-env-changed=PKG_CONFIG_SYSROOT_DIR

  --- stderr
  thread 'main' panicked at 'Не удается найти указанный файл. (os error 2)', C:\Users\user\.cargo\registry\src\github.com-1ecc6299db9ec823\opus-sys-0.2.1\build.rs:73:23
  note: run with `RUST_BACKTRACE=1` environment variable to display a backtrace
```
